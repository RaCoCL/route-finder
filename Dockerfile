FROM dordoka/tomcat
MAINTAINER "Raul Cornejo <raco@raulcornejo.com>"

RUN apt-get update && apt-get install -y \
vim

ADD /docker-conf/tomcat-users.xml /opt/tomcat/conf/
ADD /docker-conf/settings.xml /opt/tomcat/conf/
