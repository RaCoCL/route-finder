package com.routefinder;

import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.*;

import com.google.gson.Gson;

@Path("/")
public class Rest {

	/**
	 * This method receives an origin and destination and then it calculates the shortest path between both
	 * We use Dijkstra's algorithm to calculate the path
	 * @param origin
	 * @param destination
	 * @return json 
	 * 
	 */
	@Path("find_shortest/{origin}/{destination}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getShortestPath(@PathParam("origin") String origin, @PathParam("destination") String destination) {

		try {
			SimpleDirectedWeightedGraph<String, DefaultWeightedEdge> graph = buildGraph();
			List<DefaultWeightedEdge> shortestPath = DijkstraShortestPath.findPathBetween(graph, origin.toUpperCase(), destination.toUpperCase());
			String json = new Gson().toJson(shortestPath);
			return Response.ok(json, MediaType.APPLICATION_JSON).build();
		} catch (IllegalArgumentException e) {
			String json = new Gson().toJson(e.getMessage());
			return Response.status(Response.Status.BAD_REQUEST).entity(json).build();
		}

	}

	/**
	 * This method calculates all the possible paths from one origin to a destination.
	 * It's important to mention that in this case we are searching simple (non-self-intersecting) paths.
	 * We could, however change that on the getAllPaths function but I decided to ignore the 
	 * self intersecting results due to the huge ammount of possible results that it could bring and
	 * , most importantly, it seems useless to me to show paths that are self intersecting.
	 * 
	 * @param origin
	 * @param destination
	 * @return json
	 */
	@Path("find_all_paths/{origin}/{destination}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllPaths(@PathParam("origin") String origin, @PathParam("destination") String destination) {

		try {
			SimpleDirectedWeightedGraph<String, DefaultWeightedEdge> graph = buildGraph();
			AllDirectedPaths<String, DefaultWeightedEdge> pathFindingAlg = new AllDirectedPaths<>(graph);
			List<GraphPath<String, DefaultWeightedEdge>> allPaths = pathFindingAlg.getAllPaths(origin.toUpperCase(), destination.toUpperCase(),
					true, null);
			String json = new Gson().toJson(allPaths);
			return Response.ok(json, MediaType.APPLICATION_JSON).build();
		} catch (IllegalArgumentException e) {
			String json = new Gson().toJson(e.getMessage());
			return Response.status(Response.Status.BAD_REQUEST).entity(json).build();
		}

	}

	/**
	 * Here we are building manually the Graph, Vertex, Edges and it's weights. Using the JGrapht Library
	 * We could, however, in the future, change this to be build vertex dinamically and, perhaps, save
	 * graphs on a db. We should, however, change the design of the application and make use of
	 * some abstraction and interfaces, but I tried to keep this as simple as possible for now.
	 * 
	 * @return graph
	 */
	public static SimpleDirectedWeightedGraph<String, DefaultWeightedEdge> buildGraph() {

		SimpleDirectedWeightedGraph<String, DefaultWeightedEdge> graph = new SimpleDirectedWeightedGraph<String, DefaultWeightedEdge>(
				DefaultWeightedEdge.class);
		graph.addVertex("A");
		graph.addVertex("B");
		graph.addVertex("C");
		graph.addVertex("D");
		graph.addVertex("E");
		graph.addVertex("F");
		graph.addVertex("G");
		graph.addVertex("H");
		graph.addVertex("I");
		graph.addVertex("J");

		DefaultWeightedEdge edge1 = graph.addEdge("A", "B");
		graph.setEdgeWeight(edge1, 12);
		DefaultWeightedEdge edge2 = graph.addEdge("A", "D");
		graph.setEdgeWeight(edge2, 19);
		DefaultWeightedEdge edge3 = graph.addEdge("A", "E");
		graph.setEdgeWeight(edge3, 20);
		DefaultWeightedEdge edge4 = graph.addEdge("A", "G");
		graph.setEdgeWeight(edge4, 16);
		DefaultWeightedEdge edge5 = graph.addEdge("B", "C");
		graph.setEdgeWeight(edge5, 5);
		DefaultWeightedEdge edge6 = graph.addEdge("B", "D");
		graph.setEdgeWeight(edge6, 13);
		DefaultWeightedEdge edge7 = graph.addEdge("B", "I");
		graph.setEdgeWeight(edge7, 15);
		DefaultWeightedEdge edge8 = graph.addEdge("C", "D");
		graph.setEdgeWeight(edge8, 5);
		DefaultWeightedEdge edge9 = graph.addEdge("D", "E");
		graph.setEdgeWeight(edge9, 7);
		DefaultWeightedEdge edge10 = graph.addEdge("E", "F");
		graph.setEdgeWeight(edge10, 5);
		DefaultWeightedEdge edge11 = graph.addEdge("F", "A");
		graph.setEdgeWeight(edge11, 5);
		DefaultWeightedEdge edge12 = graph.addEdge("G", "F");
		graph.setEdgeWeight(edge12, 11);
		DefaultWeightedEdge edge13 = graph.addEdge("H", "A");
		graph.setEdgeWeight(edge13, 4);
		DefaultWeightedEdge edge14 = graph.addEdge("H", "B");
		graph.setEdgeWeight(edge14, 19);
		DefaultWeightedEdge edge15 = graph.addEdge("H", "G");
		graph.setEdgeWeight(edge15, 6);
		DefaultWeightedEdge edge16 = graph.addEdge("I", "J");
		graph.setEdgeWeight(edge16, 10);
		DefaultWeightedEdge edge17 = graph.addEdge("I", "H");
		graph.setEdgeWeight(edge17, 21);
		DefaultWeightedEdge edge18 = graph.addEdge("J", "B");
		graph.setEdgeWeight(edge18, 7);
		DefaultWeightedEdge edge19 = graph.addEdge("J", "C");
		graph.setEdgeWeight(edge19, 15);

		return graph;
	}

}