# README #

This is an Application which uses Jersey to serve Rest Requests. And it also counts with an html page which consumes the Rest services to build an appropriate and `human friendly` response.

----------
##Summary

Some of the technologies we use here are:

- maven
- jersey
- JGrapht
- JQuery
- Cytoscape.js

This requires Java 8 since JGrapht 1.0.0 won't work on earlier versions.

----------
##Setup

As a huge fan of Docker, I dockerized this project...so if you want to deploy this with Docker, follow the instructions:

###requirements

You'll need:

- Docker
- Maven

###Deploy to Docker Instructions

- Clone this repository
- You'll find that there's a `Dockerfile` on the root path of the project.
	Run this commands to build and start your Docker image:
	- `docker build -t raco/tomcat .` (include the dot at the end)
	- `docker run -p 8080:8080 raco/tomcat`

That's it! you have Tomcat 8 up and running on your machine.

Now go to the `/route-finder/routefinder/` folder of the project. You'll find that there's a `pom.xml` file in there.

Run this maven command to deploy the project inside of the Docker Container:

- `mvn tomcat7:deploy`

That's all! once it finishes the project will be up and running on:

`http://localhost:8080/routefinder/form/all`

and

`http://localhost:8080/routefinder/form/shortest`

###Others?

If you don't want to use docker, you can use the repo as a maven project in Eclipse and deploy it as you want. 
Otherwise, you can just download the war file I added to the Repository and deploy it on the webserver you like.

### About the application ###

I made this application as simple as possible. Trying to show Java and Javascript knowledge. I didn't want to make my own implementation of the Graph algorithms given that those algorithms are very popular and it would be like reinvent the wheel again. Instead I just made use of the Java Library `JGrapht` and the Javascript library `Cytoscape`.

Also the Java Application is a Restful application which makes it be available to be used in anyway it's desired, security, auth, and other implementations could easily be made in the future with this Service Orienthed approach.

Graphs are built manually, this could also be improved if required with the use of a Database to hold more vertex and edges.

If you have any questions, contact me on `raco@raulcornejo.com`
